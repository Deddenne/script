#!/bin/bash

# Copier le fichier de configuration sur le serveur
cp ./config__files/ssh/sshd_config /etc/ssh/sshd_config
cp ./config__files/ssh/Banner /etc/Banner

# Redemarrer le service sshd
systemctl restart sshd

# Créer un utilisateur
# adduser Deddenne
# usermod -aG sudo Deddenne


# Avec l'aide de Rami, Ayman & Christopher pour créer les utilisateurs

# Créer un utilisateur
printf "entrer le nom de l'utilisateur : \n"
read user
adduser -m -d/home/$user -s /bin/bash $user && passwd $user && usermod -aG sudo $user && echo -e $user "est devenu un superuser !\n"

# Demande la clé à l'user
printf "donne moi ta clé public \n"
read key
mkdir /home/$user/.ssh/
echo $key >> /home/$user/.ssh/authorized_keys && echo "ta clé a été ajouté. \n"
#cat key.txt >> ~/.ssh/authorized_keys && echo "ta clé a été ajouté. \n"
rm key.txt


